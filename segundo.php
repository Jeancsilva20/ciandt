<?php

class segundo
{
    private $mordido = array();
    private $quantidade = 10;

    public function segundo()
    {
        do {
            ///entra no fluxo e decide se for mordido ou não
            if ($this->foiMordido())
                echo 'Joãozinho mordeu o seu dedo!<br>';
            else
                echo 'Joaozinho NAO mordeu o seu dedo!<br>';

            //ira executar até atingir o valor max da variavel quantidade
        } while (count($this->mordido) != $this->quantidade);
    }

    public function foiMordido()
    {
        do {
            //faz um random entre 1 e o valor de quantidade
            $morder = rand(1, $this->quantidade);

            //verifica se esse valor ja não foi escolhido
            //nem sei se era pra fazer isso, fiquei meio confuso na verdade
        } while (in_array($morder, $this->mordido));

        //guarda a posicao para verificar na próxima passagem
        //isso garante que o rando vai sempre resultar em 50% das vezes para os dois
        //e em ordem aleatória
        $this->mordido[] = $morder;

        // se for par não morde, se for impar morde
        if ($morder % 2 == 1)
            return true;
        return false;

    }

}
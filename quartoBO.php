<?php
/**
 * Created by PhpStorm.
 * User: Jean
 * Date: 01/11/2018
 * Time: 07:47
 */


//faltou a verificação de existentes, fiquei quase toda a semana em SP, acabei fazendo hoje
//dia 01/11 de manhã, então não vou conseguir fizalizar, mas eu faria o seguinte:

//marquei as linhas com ';' e os valores com '|' e dou explode para um array comum quando
//vou ler, então eu guardaria a chave também dentro do arquivo
// |login:teste|senha:teste|, com isso eu consigo recuperar um array de chave=>valor
// e poderia validar se já foi inserido;


//define o nome do arquivo
$name = 'arquivo.txt';

//recebe post e monta o array
$recebidos [] = array(
    "nome" => $_POST['nome'],
    "sobrenome" => $_POST['sobrenome'],
    "email" => $_POST['email'],
    "tel" => $_POST['tel'],
    "login" => $_POST['login'],
    "senha" => md5($_POST['senha'])
);

//abro o arquivo em modo leitura
$file = fopen($name, 'r');

//isso é só pra dar fim em um warning chato
if (filesize($name) > 0)
    $conteudo = fread($file, filesize($name));
//aqui em cima eu capturo o conteudo já escrito

//separo por linhas, cada uma representa uma posição do array
$conteudo = explode(";", $conteudo);

foreach ($conteudo as $value) {
    //aqui eu separo por valor das linhas e adiciono no array já montado
    $value = explode("|", $value);
    if (count($value) > 1)
        $recebidos [] = array(
            "nome" => $value[0],
            "sobrenome" => $value[1],
            "email" => $value[2],
            "tel" => $value[3],
            "login" => $value[4],
            "senha" => $value[5]
        );
    //isso nem era necessário, se eu colocasse o fopen em modo 'a' eu consigo escrever
    //no fim da linha em modo incremental, mas preciso receber os valores de volta
    //para poder verificar se ja existem
}
//mudo para modo escrita
$file = fopen($name, 'w');

//escrevo de volta tudo que recebi e os novos
foreach ($recebidos as $array) {
    foreach ($array as $key => $value)
        fwrite($file, $value . "|");
    //aqui eu marco com pipe cada valor

    //aqui eu marco com ponto e virgula o final de cada linha
    fwrite($file, ";");
}
//fecho o arquivo
fclose($file);
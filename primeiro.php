<?php
/**
 * Created by PhpStorm.
 * User: Jean
 * Date: 01/11/2018
 * Time: 07:37
 */

class primeiro
{
    public function primeiro()
    {
        //insere valores no array
        $location = array(
            'Brasil' => 'Brasilia',
            'EUA' => 'Washington',
            'Espanha' => 'Madri',
            'Canada' => 'Ottawa',
            'Argentina' => 'Buenos Aires',
            'Australia' => 'Camberra',
            'Cuba' => 'Havana',
            'Russia' => 'Moscou'
        );
        //ordena por capital
        asort($location);

        //descarrega resultado
        foreach ($location as $key => $value) {
            echo "A capital de " . $key . " é " . $value . "<br>";
        }
    }
}